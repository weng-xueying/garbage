# Garbage
## 小白垃圾分类-小程序

### 项目介绍

小程序使用了云开发，功能主要包括文字识别、语音识别、拍照识别、垃圾分类小测试、积分排行榜，垃圾分类介绍等功能。

小程序大赛参赛作品---仅供学习参考

已获得 2021中国高校计算机大赛 

微信小程序应用开发赛 华北赛区二等奖

### 小程序二维码  

小程序已通过审核，发布上线

![小程序二维码](https://gitee.com/weng-xueying/garbage/raw/master/img-xcx.jpg))

### 界面展示

![界面展示](https://gitee.com/weng-xueying/garbage/raw/master/img-zs.jpg)

### 思维导图

![思维导图](https://gitee.com/weng-xueying/garbage/raw/master/img-swdt.png)



### 安装教程

云函数中，audioRec是语音识别、baiduAccessToken是图像识别，

在云函数的index.js中的，apiKey和secretKey需要换成自己的方可使用

**cloudfunctions\audioRec\index.js 的 12-14行**

var APP_ID = ""
var API_KEY = ""
var SECRET_KEY = ""

**cloudfunctions\baiduAccessToken\index.js  的 5行 、7行**

let apiKey = '',

secretKey = '',



#### 百度AI识别设置

需要进入百度识图的官网，去注册账号
[http://ai.baidu.com/ai-doc/IMAGERECOGNITION/8k3e7f69o](http://ai.baidu.com/ai-doc/IMAGERECOGNITION/8k3e7f69o)

注册好以后去创建一个应用

应用类型：工具应用

接口选择：【图像识别】和【语音技术】的全部选中

创建成功之后就能看见AppID、apiKey、 secretKey 了，在两个云函数对应位置替换即可

##### 配置安全域名

| 服务器配置           |                                          说明 |
| :------------------- | --------------------------------------------: |
| request合法域名      | https://aip.baidubce.comhttps://log.aldwx.com |
| socket合法域名       |                        wss://aip.baidubce.com |
| uploadFile合法域名   |                      https://aip.baidubce.com |
| downloadFile合法域名 |                      https://aip.baidubce.com |
| udp合法域名          |                        udp://aip.baidubce.com |

### 云数据库

云数据库主要包含

users表，用于存储用户信息【排行榜、我的】

users_feedback表用于存储用户反馈【用户反馈】

garbage_sort表用于存储垃圾分类，包含字段id、type_id、garbage、category

### 交互界面基础依赖

项目使用了小程序云开发

阿里巴巴矢量图标库iconfont

百度智能云的图像识别和语音技术

### 参考文档

[微信开放文档 ](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)

[微信小程序云开发教程：语音识别源码教学_哔哩哔哩 ](https://www.bilibili.com/video/BV1mv411C7Pv)


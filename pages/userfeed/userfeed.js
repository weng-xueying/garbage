// pages/userfeed/userfeed.js
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  formSubmit:function(e) {
    var that = this
    if(e.detail.value.input.length < 6){
      wx.showModal({
        title:'提示',
        content: "提交失败，少于6个字",
        showCancel: false,
      })
    } else if(e.detail.value.radio == ""){
      wx.showModal({
        title:'提示',
        content: "请选择反馈类型",
        showCancel: false,
      })
    }else{
      db.collection('users_feedback').add({
        data: {
          feedbackType: e.detail.value.radio,
          feedback: e.detail.value.input,
        },
        success: function (res) {
          wx.showToast({
            title: '提交成功，感谢您的反馈',
            icon: 'none',
            duration: 1500//持续的时间
          })
          console.log(res._id)
        },
        fail: console.error
      })
      that.jumpBack()
    }
  },

  /** 跳转页面 */
  jumpBack:function(){
    wx.switchTab({
      url: '../user/user',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
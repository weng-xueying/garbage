// pages/classify.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  btnChange:function (e) {
    console.log(e.currentTarget.id)
    app.globalData.classifyId = e.currentTarget.id
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          height: res.statusBarHeight
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  gotoPage: function(event){ 
    const number = event.target.id;//1或者2得到点击了按钮1或者按钮2 
    const url = "/pages/index/talkPage" + number;//得到页面url   
    wx.navigateTo({ 
    url: url,   
    }) }
})
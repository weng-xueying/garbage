//index.js
//获取应用实例
const app = getApp()
const db = wx.cloud.database()

Page({
  data: {
    openId:''
  },

  onLoad: function (options) {
    var that = this
    wx.cloud.callFunction({
      name: 'demo',
      complete: res => {
        var openid = res.result.event.userInfo.openId
        that.setData({
          openId: openid
        })
        // 根据openid查询是否授权
        that.queryAuthByOpenid(that.data.openId)
      }
    })
  },

  queryAuthByOpenid: function (openid) {
    const db = wx.cloud.database()
    db.collection('users')
    .where({
        _openid: openid
      })
      .get({
        success(res) {
          if (res.data.length != 0 && res.data[0].auth) {
            app.globalData.auth = true
            // 已经授权，直接跳转首页
            wx.switchTab({
              url: '../home/home',
            })
          } else {
            // 未授权，停在授权界面
            console.log(res.data.length)
          }
        }
      })
  },

  add: function(option) {
    var that = this
    const db = wx.cloud.database()
    db.collection('users').where({_openid: that.data.openId}).get({
      success(res) {
        console.log(res)
        if (res.data.length != 0 && res.data[0].auth){
          console.log("已经授权")
        }
        else{
          db.collection('users').add({
            data: {
              auth: true,
              avatarUrl: option.userInfo.avatarUrl,
              nickName: option.userInfo.nickName,
              score: 20,
            },
            success: function (res) {
              console.log(res._id)
            },
            fail: console.error
          })
        }
      }
    })
  },

  empower: function(res) {
    var that = this
    wx.getUserProfile({
      desc: '获取你的昵称、头像、地区及性别',
      success: res => {
        app.globalData.auth = true
        that.add(res)
      },
      fail: res => {
        console.log(res)
        //拒绝授权
        wx.showToast({
          title: '数据获取失败',
          icon: 'none', 
          duration: 2000
        });
        return;
      }
    });
    console.log(that.data.openId)
    setTimeout(() => {
      wx.switchTab({
        url: "../home/home"
      })
    },2000)
  },
})

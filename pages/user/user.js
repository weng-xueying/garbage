// pages/user/user.js
const app = getApp()
const db = wx.cloud.database()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    openId: "",
    nickName: '',
    avatarUrl: '',
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    // 获取openId
    wx.cloud.callFunction({
      name: 'demo',
      complete: res => {
        var openId = res.result.event.userInfo.openId
        that.setData({
          openId: openId
        })
        that.queryUser(openId)
      }
    })
  },

  /*  查询用户信息  */
  queryUser(openId) {
    var that = this
    db.collection('users')
    .where({
        _openid: openId
      })
    .get({
      success(e) {
        that.setData({
          nickName: e.data[0].nickName,
          avatarUrl: e.data[0].avatarUrl,
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },


})
// pages/mypost/mypost.js
const db = wx.cloud.database()
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {
    score: 0,
    myrank: 0,
    total: 0
  },

  /*  查询我的score  */
  queryScore(openId) {
    var that = this
    db.collection('users')
    .where({
        _openid: openId
      })
    .get({
      success(e) {
        var score = e.data[0].score
        that.setData({
          score: score
        })
        that.countRank(score)
      }
    })
  },

  /*  查询我的排名  */
  countRank() {
    wx.cloud.callFunction({
      name: 'getUserRank',
      complete: res => {
        this.setData({
          myrank: res.result.rank
        })
      }
    })
  },
  getTotalUsers(){
    var total = 0
    wx.cloud.callFunction({
      name: 'getTotalUsers',
      complete: res => {
        total = res.result.total
        console.log(total)
        this.setData({
          total: total
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.id)
    this.queryScore(options.id)
    this.getTotalUsers()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
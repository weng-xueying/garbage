// pages/rank/rank.js
const db = wx.cloud.database()
const _ = db.command
Page({
  /**
   * 页面的初始数据
   */
  data: {
    flag: false,
    remind: "继续努力，你也会成为榜单第1人!",
    score: 0,
    myrank: 0,
    details: [],
    user_Rank:[],//用户头像、昵称
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    // 获取openId
    wx.cloud.callFunction({
      name: 'demo',
      complete: res => {
        var openId = res.result.event.userInfo.openId
        that.queryScore(openId)
      }
    })
    //分数降序查询前20
    db.collection('users').orderBy('score', 'desc').get().then(res => {
      that.setData({
        user_Rank: res.data,
        ['user_Rank[0].sign']: 1
      })
      for(let i=1; i<res.data.length; i++) {
        if(res.data[i].score == res.data[i-1].score){
          that.setData({
            ['user_Rank['+ i+'].sign']: that.data.user_Rank[i-1].sign
          })
        }else{
          that.setData({
            ['user_Rank['+ i+'].sign']: that.data.user_Rank[i-1].sign + 1
          })
        }
      }
    })
  },

  /*  查询我的score  */
  queryScore(openId) {
    var that = this
    db.collection('users')
    .where({
        _openid: openId
      })
    .get({
      success(e) {
        var score = e.data[0].score
        that.setData({
          score: score
        })
        that.countRank(score)
      }
    })
  },

  /*  查询我的排名  */
  countRank(score) {
    var that = this
    // db.collection('users')
    // .where({
    //   score: _.gt(score) 
    // })
    // .count().then(res => {
    //   console.log(res.total)
    //   that.setData({
    //     myrank: res.total+1
    //   })
    // })

    wx.cloud.callFunction({
      name: 'getUserRank',
      complete: res => {
        if(res.result.rank == 1){
          that.setData({
            remind: "你简直就是文能提笔安天下，武能上马定乾坤，美貌与智慧并存，英雄和侠义的化身!",
            flag: true,
            myrank: res.result.rank,
          })
        }
        else{
          that.setData({
            myrank: res.result.rank,
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
// pages/result/result.js
const db = wx.cloud.database()
const _ = db.command
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    result: [{
      resultNum: -1,
      resultTitle: "",
      resultPick: "",
      resultAnswer: "",
      judge: false,
    }],
    count: 0,
    score: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // js获取到数据后,setData无法自动更新数据，用that
    var that = this // 备份一下this
    that.setData({
      result: app.globalData.testResult
    })
    that.countPoints(app.globalData.testResult)
  },

  countPoints:function(result){
    var that = this
    var correct = 0
    var count = 0
    for (var i = 0; i < 10; i++) {
      if(result[i].judge) {
        correct += 1
      }
    }
    count = correct
    if(count>=6) correct += 2
    if(count==10) correct += 3
    that.setData({
      count: count,
      score: correct
    })

    wx.cloud.callFunction({
      name: 'demo',
      complete: res => {
        var openId = res.result.event.userInfo.openId
        that.updataScore(openId,correct)
      }
    })
  },

  updataScore:function(openId,number) {
    db.collection('users').where({_openid: openId}).update({
      data: {
      score: _.inc(number)
      }
    }).then(res => {
      console.log(res);
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
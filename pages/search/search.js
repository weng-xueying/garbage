// pages/search/search.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    inputValue: null,
    resultList: [],
    imageSrc: '',
    display: false
  },
  search:function(inputValue){
    wx.cloud.database().collection('garbage_sort').where({
      garbage:{
        $regex:'.*' + inputValue + '.*',
        $options: '1'
      }}).get({
        success: res => {
          this.setData({
            resultList: res.data
          })
        },
      })
  },

  blur: function (e) {
    this.setData({
      inputValue: e.detail.value
    })
    this.search(this.data.inputValue);
  },

  /* （保留）：搜索展示的数据，点击是否有弹窗或提示等 */
  detail: function (e) {
    // this.save();
    // wx.navigateTo({
    //   url: '../projectDetail/projectDetail?id=' + e.currentTarget.dataset.id,
    // })
  },

  btnClick: function(event) {
    console.log(event.currentTarget.id) // 取到bindtap传的id参数
    // 取按钮的值 填入搜索框
    this.setData({
      inputValue: this.data.resultList[event.currentTarget.id]
    })
    this.search(this.data.inputValue);
    this.setData({
      display: false
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.cloud.callFunction({
      name:"demo"
    })
    // 判断上个页面是否是 test
    if (app.globalData.pageBefore) {
      this.setData({
        display: true
      })
      app.globalData.pageBefore = false // 全局变量恢复默认值
    }
    if(app.globalData.voicetf) {
      app.globalData.voicetf = false
      this.setData({
        inputValue: app.globalData.voiceResult
      })
      this.search(this.data.inputValue);
    }
    this.setData({
      resultList: app.globalData.scanResult,
      imageSrc: app.globalData.imageTemp,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
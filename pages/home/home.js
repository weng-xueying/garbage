// home/home.js
const app = getApp()
const db = wx.cloud.database()
const _ = db.command
Page({
  /**
   * 页面的初始数据
   */
  data: {
    pop: false,
    showModal1: false,
    showModal2: false,
    showModal3: false,
    showModal4: false,
    indicatorDots: true,//是否显示面板指示点
    autoplay:false,//是否自动切换
    interval:2000,//自动切换时间间隔
    duration:300,//滑动动画时长
    true:true,
    banner:[
      {url:'../../image/1.png'},
      {url:'../../image/2.png'},
      {url:'../../image/3.png'}
    ],
    currentTab: 0,
  },
  /** 跳转识图页面 */
  jump_scan:function(){
    var that = this
    if(app.globalData.auth) {
      wx.showModal({
        title: '提示',
        content:'您即将消耗10积分',
        showCancel: true,
        success: function(res){
          if (res.confirm){
            that.queryScore()
          }
          if (res.cancel) {
            console.log("您点击了取消")
          }
        }
      })
    }
    else{
      wx.showModal({
        title: '温馨提示',
        content:'请授权后继续操作',
        showCancel: true,
        cancelText:'确定',
        confirmText:'去授权',
        success: function(res) {
          if (res.confirm){
            that.empower()
          }
          if (res.cancel) {
            console.log("确定")
          }
        }
      })
    }
  },
  /** 查询我的score */
  queryScore:function() {
    // 获取openId
    var openId = ''
    var that = this
    wx.cloud.callFunction({
      name: 'demo',
      complete: res => {
        openId = res.result.event.userInfo.openId
        db.collection('users')
        .where({
            _openid: openId
          })
        .get({
          success(e) {
            var score = e.data[0].score
            if(score >= 10){
              that.updataScore(openId)
            }
            else{
              wx.showToast({
                title: '积分不足',
                icon: 'none',
                duration: 1500//持续的时间
              })
            }
          }
        })
      }
    })
  },
  /** 更新分数 */
  updataScore:function(openId){
    db.collection('users').where({_openid: openId}).update({
      data: {
        score: _.inc(-10)
      }
    }).then(res => {
      console.log(res);
    })
    wx.navigateTo({
      url: '../scan/scan',
    })
  },
  /** 跳转测试页面 */
  jump_test:function(){
    wx.navigateTo({
      url: '../test/test',
    })
  },
  /** 跳转语音页面 */
  jump_voice:function(){
    wx.navigateTo({
      url: '../voice/voice',
    })
  },
  /** 跳转排名页面 */
  jump_rank:function(){
    wx.navigateTo({
      url: '../rank/rank',
    })
  },
  /** 自定义弹框组件 */
  cardModal1:function(){
    this.setData({
      showModal1: true,
      pop: true,
      show: true
    })
  },
  cardModal2:function(){
    this.setData({
      showModal2: true,
      pop: true
    })
  },
  cardModal3:function(){
    this.setData({
      showModal3: true,
      pop: true
    })
  },
  cardModal4:function(){
    this.setData({
      showModal4: true,
      pop: true
    })
  },
  /** 添加用户 */
  add: function(option) {
    app.globalData.auth = true
    db.collection('users').add({
      data: {
        auth: true,
        avatarUrl: option.userInfo.avatarUrl,
        nickName: option.userInfo.nickName,
        score: 20,
      },
      success: function (res) {
        console.log(res._id)
      },
      fail: console.error
    })
  },
  /** 调用授权接口 */
  empower: function(res) {
    var that = this
    wx.getUserProfile({
      desc: '获取你的昵称、头像、地区及性别',
      success: res => {
        app.globalData.auth = true
        that.add(res)
      },
      fail: res => {
        console.log(res)
        //拒绝授权
        wx.showToast({
          title: '数据获取失败',
          icon: 'none', 
          duration: 2000
        });
        return;
      }
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          height: res.statusBarHeight
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // tarbar跳转弹窗更新
    this.setData({
      showModal1: false,
      showModal2: false,
      showModal3: false,
      showModal4: false
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  switchNav: function(res) {
    if (this.data.currentTab === res.target.dataset.current) {
      return false;
    } else {
      this.setData({
        currentTab: res.target.dataset.current
      });
    }
  },
  switchNav2: function(res) {
    let sourc = res.detail.source;
    console.log("source:"+sourc)
    if(sourc=='touch'){
        if (1 === this.data.currentTab) {
          this.setData({
            currentTab: 0
          });
        } else {
          this.setData({
            currentTab: 1
          });
        }
    }
  }
})
// pages/voice/voice.js
var app = getApp()
// 初始化录音接口
const record_manager = wx.getRecorderManager()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    explain: "说出你想搜的垃圾",
    result: "",
    authed: false,// 授权
    recording: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.get_record_auth()
  },

  /** 获取录音授权 */
  get_record_auth(){
    var that = this;
    wx.getSetting().then(res=>{
      // 如果之前有授权
      if(res.authSetting['scope.address.record']){
        that.setData({authed:true})
      }
      else{
        // 调用录音授权接口
        wx.authorize({
          scope: 'scope.record',
        }).then(res=>{
          that.setData({authed:true})
        }).catch(err=>{
          that.cancel_auth()
        })
      }
    })
  },
  cancel_auth(){
    var that = this;
    wx.showModal({
      title:'提示',
      content:'未授权无法录音哦~',
      cancelText:'去授权',
      success:res=>{
        if(res.confirm){
          // 如果点击了"去授权"，打开设置页面
          wx.openSetting({
            success(res){
              // 如果携带了授权参数
              if(res.authSetting['scope.record']){
                that.setData({authed:true})
              }
            }
          })
        }
      }
    })
  },

  start_record(){
    const options = {
      sampleRate: 8000,
      numberOfChannels: 1,
      encodeBitRate: 48000,
      format: 'PCM'
    }
    // 启动录音
    record_manager.start(options)
    this.setData({recording:true})
    this.setData({
      explain: "正在聆听中……"
    })
  },

  stop_record(){
    record_manager.stop()
    this.setData({recording:false})
    // 获取录音音频地址
    this.bind_stop()
  },

  bind_stop(){
    var that = this
    record_manager.onStop(res=>{
      var tf = res.tempFilePath;// 录音缓存地址
      const fs = wx.getFileSystemManager()// 读取文件
      fs.readFile({
        filePath:tf,
        success(res){
          const buffer = res.data// 存储到buffer
          that.audio_rec(buffer)// 将录音文件传到云函数的接口
        }
      })
    })
  },

  audio_rec(data){
    var that = this;
    wx.showLoading({
      title: '语音识别中',
    })
    wx.cloud.callFunction({
      name: 'audioRec',
      data: {data}
    }).then(res => {
      console.log(res)
      // 如果云函数和云识别调用成功
      if(res.errMsg=="cloud.callFunction:ok"&& res.result.err_no==0){
        var result_list = res.result.result
        that.setData({
          result:(result_list.join('')).replace(/。/g,'')
          // 百度AI返回数据带'。',把'。'替换掉
        })
        if(that.data.result != "") {
          that.setData({
            explain: that.data.result
          })
          setTimeout(function(){
              that.setData({
                explain: "说出你想搜的垃圾"
              })
              app.globalData.voiceResult = that.data.result
              app.globalData.voicetf = true
              wx.redirectTo({
                url: '../search/search',
              })
            }, 2000);
        }
        else if (that.data.result == "") {
          that.setData({
            explain: "对不起，我没有听清楚"
          })
          setTimeout(function(){
            that.setData({
              explain: "说出你想搜的垃圾"
            })
          }, 2000);
        }
        wx.hideLoading()
      }
      else{
        wx.showToast({
          title: '识别失败',
          icon: 'none'
        })
      }
    }).catch(err => {
      console.log("err,", err)
      wx.showToast({
        title: '识别失败',
        icon: 'none'
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
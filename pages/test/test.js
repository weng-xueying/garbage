// pages/test/test.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    swiperHeight: 0,//初始时swiper的高度是0
    number: 0,
    garbage:[],
    currentTab: 0,
  },
  //父组件取到子组件传来的值,取e.detail.子组件传的变量（父组件wxml的组件标签中： bind子组件="addion" ）
  addion(e) {
    // 这里就是子组件传过来的内容了
    if(e.detail[0].mess){
      setTimeout(() => {
        if(this.data.number!=9){
          this.setData({
            number: this.data.number+1
          })
        }
        else{
          this.redirectBtn()
        }  
      }, e.detail[1].time);
    }
  },

  // 截获竖向滑动------禁止轮播图滑动
  catchTouchMove:function(res){
    return false
  },

  changeIn(e) {
    if(e.detail.source === 'touch') {
      this.setData({
        number : e.detail.current + 1
      })
    }
  },

  // 关闭当前页面，跳转到应用内的某个页面。
  redirectBtn: function(){
    wx.redirectTo({
      url: '../result/result',
    })
 },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    let that = this;
    // 动态获取屏幕高度，设置swiper的高度
    wx.getSystemInfo({
      success: (res) => {
        let clientHeight = res.windowHeight;
        let clientWidth = res.windowWidth;
        let ratio = 750 / clientWidth;//计算为百分比
        let rpxHeight = ratio * clientHeight;
        that.setData({
          swiperHeight:rpxHeight//将计算好的高度给定义好的值
        })
      },
    })
    this.search();
  },

  /* 随机获取测试题目 */
  search:function(){
    wx.cloud.callFunction({
      name: 'getTestList',
      complete: res => {
        this.setData({
          garbage: res.result.garbage
        })
        wx.hideLoading()
      }
    })

    // console.log(this.data.garbage)
    /*云数据库查询-and和or的用法用例 */
    // var count = Math.ceil(Math.random()*590)
    // const test = wx.cloud.database().collection('garbage_sort').where({
    //   id:_.and(_.gt(count),_.lt(count+11))
    // }).get().then((data) => { 
    // })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
// pages/userus/userus.js
// pages/userfeed/userfeed.js
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    versionNumber: "1.0.0"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    db.collection('version').get().then((res) => {
      this.setData({
        versionNumber: res.data[0].versionNumber
      })
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

})
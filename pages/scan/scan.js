// pages/scan/scan.js
var http = require('../../utils/http.js')
var md5 = require('../../utils/md5.js')
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    accessToken: "",
    isCamera: true,
    btnTxt: "拍照",
    src: "",
    tempImagePath: "", // 拍照的临时图片地址
    base64Image: ""
  },
  takePhoto() {
    var that = this
    if (this.data.isCamera == false) {
      this.setData({
        isCamera: true,
        btnTxt: "拍照",
      })
      return
    }
    this.ctx.takePhoto({
      quality: 'high',
      success: (res) => {
        this.setData({
          src: res.tempImagePath,
          isCamera: false,
          btnTxt: "重拍",
          tempImagePath: res.tempImagePath
          // ------------------------------------------
        })
        console.log(this.data.tempImagePath)// wxfile://tmp_6806ca3899dbde47f5c9e85484fb1005ea0f8196072acc01.jpg

        wx.showLoading({
          title: '正在加载中',
        })
        wx.getFileSystemManager().readFile({
          filePath: res.tempImagePath,
          encoding: "base64",// 编码格式
          success: res => {
            that.req(that.data.accessToken, res.data)
            this.setData({
              base64Image: res.data
            })
            app.globalData.imageTemp = this.data.base64Image

            // console.log(res.data)//转成b64的图片
            // =====================================
          },
          fail: res => {
            wx.hideLoading()
            wx.showToast({
              title: '拍照失败,未获取相机权限或其他原因',
              icon: "none"
            })
          }
        })
      }
    })
  },
  
  error(e) {
    console.log(e.detail)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.ctx = wx.createCameraContext()// 微信小程序相机组件
    var time = wx.getStorageSync("time")// wx.getStorageSync(KEY)同步缓存
    // 对部分缓存设置时效性，在它到期之后自动删除。
    var curTime = new Date().getTime() // 获取当前时间 ,转换成JSON字符串序列
    var timeInt=parseInt(time) // parseInt() 函数可解析一个字符串,并返回一个整数。
    var timeNum = parseInt((curTime - timeInt) / (1000 * 60 * 60 * 24))
    // 两者相减可以得到相应相差多少毫秒值
    // 除于1000  就能够得到 相差的秒数
    // timeNum为相差天数
    // console.log("=======" + timeNum) 
    var accessToken = wx.getStorageSync("access_token")
    // console.log("====accessToken===" + accessToken + "a")
    if (timeNum > 28 || (accessToken == "" ||
        accessToken == null || accessToken == undefined)) {
      this.accessTokenFunc()// ===================
    } else {
      this.setData({
        accessToken: wx.getStorageSync("access_token")
      })
    }
  },

  accessTokenFunc: function() {
    var that = this
    console.log("accessTokenFunc is start")
    wx.cloud.callFunction({
      name: 'baiduAccessToken',
      success: res => {
        // console.log("==baiduAccessToken==" + JSON.stringify(res))
        console.log(res)
        that.data.accessToken = res.result.data.access_token
        wx.setStorageSync("access_token", res.result.data.access_token)
        wx.setStorageSync("time", new Date().getTime())
      },
      fail: err => {
        wx.clearStorageSync("access_token")
        wx.showToast({
          icon: 'none',
          title: '调用失败,请重新尝试',
        })
      }
    })
  },

  redirectBtn: function(){
    wx.redirectTo({
      url: '../search/search',
    })
 },

  req: function(token, image) {
    var that = this
    http.req("https://aip.baidubce.com/rest/2.0/image-classify/v2/advanced_general?access_token=" + token, {
      "image": image
    }, function(res) {
  
      wx.hideLoading()// 隐藏 loading 提示框
      console.log(JSON.stringify(res))
      var code=res.data.err_code 
      if (code == 111 || code == 100 || code==110){
        wx.clearStorageSync("access_token")
        wx.clearStorageSync("time")
        that.accessTokenFunc()
        return
      }
      var num = res.result_num
      var results = res.data.result
      if (results != undefined && results != null) {
        that.setData({
          isShow: true,
          results: results,
          scanResult: results
        })
        for (var i=0; i<5; i++) {
          app.globalData.scanResult[i] = results[i].keyword
          console.log(results[i].keyword)
        }
        app.globalData.pageBefore = true
        that.redirectBtn()//跳转页面
      } else {
        wx.clearStorageSync("access_token")
        wx.showToast({
          icon: 'none',
          title: 'AI识别失败,请重新尝试',
        })
      }
    }, "POST")
  },
  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
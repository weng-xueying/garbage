const cloud = require('wx-server-sdk')
cloud.init({
  env: 'xiaoweng-garbage-9fiu1sn5f68bcc1'
})
const db = cloud.database()

exports.main = async (event, context) => {
  // 先取出集合记录总数
  const countResult = await db.collection('users').count()
  const total = countResult.total

  return {
    total: total,
  }
}
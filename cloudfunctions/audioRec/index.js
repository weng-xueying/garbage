// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'xiaoweng-garbage-9fiu1sn5f68bcc1',
  traceUser: true
})

// 初始化百度智能
var baidu = require('baidu-aip-sdk').speech

var APP_ID = ""
var API_KEY = ""
var SECRET_KEY = ""

// 云函数入口函数
exports.main = async (event, context) => {
  // 实例化语音识别接口
  var client = new baidu(APP_ID,API_KEY,SECRET_KEY)

  client.Timeout = 60000;  // 修改超时时间

  var buffer = new Buffer.from(event.data.data)

  var res = await client.recognize(buffer, 'pcm', 8000)
  console.log(res);
  return res
}
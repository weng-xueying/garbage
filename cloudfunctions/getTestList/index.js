// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: 'xiaoweng-garbage-9fiu1sn5f68bcc1'
})

const db = cloud.database()
const _ = db.command

// 云函数入口函数
exports.main = async (event, context) => {
  const countResult = await db.collection('garbage_sort').count()
  const total = countResult.total-1
  let justic = false
  let garbage = []
  /* 不重复随机数列表 */
  let randomList = []
  randomList[0] = Math.floor(Math.random()*total) + 1
  for(let i = 1; i < 10; i++) {
    let justic = true
    let random = Math.floor(Math.random()*total) + 1
    for(let j=0;j<i;j++){
        if(randomList[j] == random){
          i = i-1
          justic = false
          break
        }
    }
    if(justic){
      randomList[i] = random
    }
  }
  console.log(randomList)
  /* 生成题目 */
  for(let k = 0; k < 10; k++) {
    await db.collection('garbage_sort').where({
      id: randomList[k]
    }).get().then((data) => { 
      garbage.push( {
        title: data.data[0].garbage,
        type: data.data[0].type_id
      })
    })
  }
  return {
    garbage: garbage
  }
}
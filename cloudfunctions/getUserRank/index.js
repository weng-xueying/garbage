const cloud = require('wx-server-sdk')
cloud.init({
  env: 'xiaoweng-garbage-9fiu1sn5f68bcc1'
})
const db = cloud.database()
const MAX_LIMIT = 100
console.log(MAX_LIMIT)
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const countResult = await db.collection('users').count()
  const total = countResult.total
  // 计算需分几次取
  const batchTimes = Math.ceil(total / 100)
  // 承载所有读操作的 promise 的数组
  const tasks = []
  for (let i = 0; i < batchTimes; i++) {
    const promise = await db.collection('users').orderBy('score', 'desc').skip(i * MAX_LIMIT).limit(MAX_LIMIT).get()
    tasks.push(promise)
  }
  const array = [];
  // const array = tasks[0].data;
  const rankArray = [];

  for(let j=0; j<batchTimes ;j++){
    // ... 代表解构，将数据一个一个拿出来。
    // array.push(tasks[j].data)
    array.push(...tasks[j].data)
  }

  for(let i = 0; i<array.length; i++) {
    if(i == 0) {
      rankArray.push(array[0].score)
    } else {
      if(rankArray[rankArray.length - 1] != array[i].score) {
        rankArray.push(array[i].score) 
      }
    }
  }

  const user = await db.collection('users').where({_openid: wxContext.OPENID}).get()
  const score = user.data[0].score;
  let rank = 0;
  for( let i = 0 ; i < rankArray.length ; i++) {
      if(rankArray[i] == score) {
        rank = i + 1;
        break;
      }
  }

  return {
    rank: rank
  }
}
// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  // 指定当前环境变量
  env: 'xiaoweng-garbage-9fiu1sn5f68bcc1'
})
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()

  return {
    event,
    openid: wxContext.OPENID,
    appid: wxContext.APPID,
    unionid: wxContext.UNIONID,
  }
}
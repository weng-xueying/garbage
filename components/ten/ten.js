// components/ten/ten.js
var app = getApp()
Component({
  /**
   * 组件的属性列表
   */

  //父组件给子组件传值
  properties: {
    title:{
      type:String,
      value: 'title',
    },
    num:{
      type:Number,
      value: 1,
    },
    answer:{
      type:Number,
      value:-1,
    }
  },
  /**
   * 页面的初始数据
   */
  data: {
    // ten_title:this.properties.title,
    setbtn:{},
    // num:{},
    btn_num:{},
    numb:0,
    disabled:false,
    btn:['可回收垃圾','有害垃圾','厨余垃圾','其他垃圾'],
    contime: 0
  },
  /**
   * 组件的方法列表
   */
  methods: {
    click:function(res){
      this.setData({
        //btn_num是button的id
        btn_num: res.target.dataset.current,
        numb: this.data.numb+1,
        disabled: true,
      });
      if(this.data.answer == this.data.btn_num){
        // console.log(this.properties.num)
        app.globalData.testResult[this.properties.num-1] = {
          resultNum: this.properties.num, //第几题
          resultTitle: this.properties.title,// 题目
          resultPick: this.data.btn[this.data.btn_num],// 选的
          resultAnswer: this.data.btn[this.data.answer],// 正确
          judge: true,// 对错
        }
        wx.showToast({
          title: '回答正确',
          duration: 400//持续的时间
        })
        this.setData({
          contime: 400
        })
      }
      else{
        /*待优化：回答错误时，弹窗关闭再翻页 */
        app.globalData.testResult[this.properties.num-1] = {
          resultNum: this.properties.num, //第几题
          resultTitle: this.properties.title,// 题目
          resultPick: this.data.btn[this.data.btn_num],// 选的
          resultAnswer: this.data.btn[this.data.answer],// 正确
          judge: false,// 对错
        }
        wx.showModal({
          title:'答案错误',
          content: this.data.title+"是"+this.data.btn[this.data.answer],
          showCancel: false,
        })
        this.setData({
          contime: 1500
        })
      }
      // 给父组件传值，需要在父组件的wxml绑定add（bindadd="父组件对应的方法名"）,mess是传的值
      // 使用triggerEvent传递多个参数时，需要以数组的形式传参
      this.triggerEvent('add',[{mess: true},{time: this.data.contime}])
    },
  },
})